#include <websocketpp/config/asio_no_tls_client.hpp>
#include <websocketpp/client.hpp>

#include <websocketpp/common/thread.hpp>
#include <websocketpp/common/memory.hpp>

#include <cstdlib>
#include <iostream>
#include <map>
#include <string>
#include <sstream>


namespace pg {
namespace ws {

/* -------------------------------------------------------------------------- */

typedef websocketpp::client<websocketpp::config::asio_client> client;

// pull out the type of messages sent by our config
typedef websocketpp::config::asio_client::message_type::ptr message_ptr;


class ConnectionMeta
{
public:
    typedef websocketpp::lib::shared_ptr<ConnectionMeta> ptr;

    ConnectionMeta(int id, websocketpp::connection_hdl hdl, std::string uri)
      : id_(id)
      , hdl_(hdl)
      , status_("Connecting")
      , uri_(uri)
      , server_("N/A")
    {}

	void on_message(client* c, websocketpp::connection_hdl hdl, message_ptr msg);

    void on_open(client * c, websocketpp::connection_hdl hdl);

    void on_fail(client * c, websocketpp::connection_hdl hdl);
    
    void on_close(client * c, websocketpp::connection_hdl hdl);

    websocketpp::connection_hdl get_hdl() const {
        return hdl_;
    }
    
    int get_id() const {
        return id_;
    }
    
    std::string get_status() const {
        return status_;
    }

    friend std::ostream & operator<< (std::ostream & out, ConnectionMeta const & data);
private:
    int id_;
    websocketpp::connection_hdl hdl_;
    std::string status_;
    std::string uri_;
    std::string server_;
    std::string error_reason_;
};

std::ostream & operator<< (std::ostream & out, ConnectionMeta const & data);

/* -------------------------------------------------------------------------- */


class Client
{
public:
    Client();
    ~Client();

    int connect(std::string const & uri);
    void close(int id, websocketpp::close::status::value code, std::string reason);

    // send to FIRST connection
    void sendRaw(const std::string& msg);
	void sendBin(void* data, size_t len);

    ConnectionMeta::ptr get_metadata(int id) const;
private:
    typedef std::map<int, ConnectionMeta::ptr> con_list;

    client endpoint_;
    websocketpp::lib::shared_ptr<websocketpp::lib::thread> thread_;

    con_list connection_list_;
    int next_id_;
};

/* -------------------------------------------------------------------------- */


} // namespace ws
} // namespace pg

