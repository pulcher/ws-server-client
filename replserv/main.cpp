#include "server.h"

#include <thread>
#include <mutex>
#include <sstream>

using namespace ws;

Server server;

std::mutex writeMut;

class MyListener : public Server::Listener
{
public:
    void onMessage(WsConnection con, WsServer::message_ptr msg) override
    {
        std::lock_guard<std::mutex> guard{writeMut};

        std::cout << "Message received: ";
        std::cout << msg->get_payload();

        // Reprint prompt
        std::cout << "\n\nchibi> ";
        std::cout.flush();
    }

    void onConnect(WsConnection con) override
    {
        std::ostringstream clientURIStr{""};

        if(con)
        {
            clientURIStr << "[" << con->get_uri()->str() << "] ";

            // clientURIStr << "[" << con->get_subprotocol() << "|";
            // for(auto s: con->get_requested_subprotocols())
            //     clientURIStr << s << ", ";
            // clientURIStr << "]";
        }

        std::lock_guard<std::mutex> guard{writeMut};
        std::cout << "Client connected! " << clientURIStr.str();

        // Reprint prompt
        std::cout << "\nchibi> ";
        std::cout.flush();
    }

    void onDisconnect(WsConnection con) override
    {
        std::ostringstream clientURIStr{""};

        if(con)
            clientURIStr << "[" << con->get_uri()->str() << "]";

        std::lock_guard<std::mutex> guard{writeMut};
        std::cout << "Client disconnected! " << clientURIStr.str();

        // Reprint prompt
        std::cout << "\nchibi> ";
        std::cout.flush();
    }
};


void readInput()
{
    while (true)
    {
        {   std::lock_guard<std::mutex> guard{writeMut};
            std::cout << "chibi> ";
        }

        std::string text;
        getline(std::cin, text);
        if (text.length() > 0)
        {
            // auto ret3 = schemeEval(text);
            server.broadcastMessage(text);
            std::cout << "Sending!\n";
            // std::cout << text << "\n";
            // std::cout << ret3 << "\n";
        }
    }
}


// Usage
int main()
{
    std::thread t(readInput);
    server.setListener(new MyListener());
    server.run(9002);
}

