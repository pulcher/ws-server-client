#pragma once

#include <mutex>
#include <condition_variable>


namespace bc
{


class Semaphore
{
public:
  Semaphore(int count_ = 0) : count{count_} {}

  void notify()
  {
      std::unique_lock<std::mutex> lck(mtx);
      ++count;
      cv.notify_one();
  }

  void wait()
  {
      std::unique_lock<std::mutex> lck(mtx);
      cv.wait(lck, [=] { return 0 < count; });
      --count;
  }

private:
    std::mutex mtx;
    std::condition_variable cv;
    int count;
};


class SemaphoreBinary
{
public:
    //explicit SemaphoreBinary(int init_count = count_max)
    explicit SemaphoreBinary(int init_count = 0)
      : count_(init_count) {}

    // P-operation / acquire
    void wait()
    {
        std::unique_lock<std::mutex> lk(m_);
        cv_.wait(lk, [=]{ return 0 < count_; });
        --count_;
    }
    bool try_wait()
    {
        std::lock_guard<std::mutex> lk(m_);
        if (0 < count_) 
        {
            --count_;
            return true;
        }
        else
        {
            return false;
        }
    }
    // V-operation / release
    void signal()
    {
        std::lock_guard<std::mutex> lk(m_);
        if (count_ < count_max) 
        {
            ++count_;
            cv_.notify_one();
        }
    }

    // Lockable requirements
    void lock() { wait(); }
    bool try_lock() { return try_wait(); }
    void unlock() { signal(); }

private:
    static const int count_max = 1;
    int count_;
    std::mutex m_;
    std::condition_variable cv_;
};


} // namespace bc
