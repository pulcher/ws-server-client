

#include "SDL.h"
#include <time.h>

#include <random>
#include <functional>
#include <vector>
#include <algorithm>
#include <iterator>
#include <iostream>

#include <thread>
#include <mutex>

#include <string>
#include <sstream>

#include "bcSemaphore.h"

#include "my_cstructs.h"
#include "chibi_interface.h"

#ifdef NETWORK
#include "client.h"
#endif

using bite::Rectangle;
using bite::Vec2;
using bite::Color;

using Rectangles = std::vector<bite::Rectangle>;


#define SCREEN_WIDTH 320
#define SCREEN_HEIGHT 480


std::mutex chibiMutex;

// Global chibi context
sexp ctx;

bc::SemaphoreBinary sema;
bc::SemaphoreBinary sema_connect;

std::string schemeEval(const std::string& expression)
{
	using namespace std;
	lock_guard<mutex> lc(chibiMutex);
	auto ret1 = sexp_eval_string(ctx, expression.c_str(), -1, NULL);
	auto ret2 = sexp_write_to_string(ctx, ret1);
	auto ret3 = sexp_string_data(ret2);
	return ret3;
}



//#include "scheme/snippet1.inl"

void dostuff(sexp ctx) {
    /* declare and preserve local variables */
    sexp_gc_var2(obj1, obj2);
    sexp_gc_preserve2(ctx, obj1, obj2);
    
    /* load a file containing Scheme code */
    //obj1 = sexp_c_string(ctx, "/path/to/source/file.scm", -1);
    //sexp_load(ctx, obj1, NULL);
    
    /* eval a C string as Scheme code */
    //sexp_eval_string(ctx, "(some scheme expression)", -1, NULL);
    obj1 = sexp_eval_string(ctx, "(+ 2 2)", -1, NULL);
    obj2 = sexp_eval_string(ctx, "(+ 1 1)", -1, NULL);
    auto x = sexp_unbox_fixnum(obj1);
    /* construct a Scheme expression to eval */
    //obj1 = sexp_intern(ctx, "my-procedure", -1);
    //obj2 = sexp_cons(ctx, obj1, SEXP_NULL);
    //sexp_eval(ctx, obj2, NULL);

    std::string str_change_rect = R"(
        (begin
	    (define r1 (car my-rects))
	    (define p (rect-pos r1))
	    (vec2-x! p 0.0) )
       )";
	//str_change_rect= std::string(snippet1_scm, snippet1_scm_len);
    //obj2 = sexp_eval_string(ctx, str_change_rect.c_str(), -1, NULL);
	schemeEval(str_change_rect);

    /* release the local variables */
    sexp_gc_release2(ctx);
}


int
randomInt(int min, int max)
{
    return min + rand() % (max - min + 1);
}

SDL_Rect toSdlRect(bite::Rectangle r)
{
    SDL_Rect sr;
    sr.x = r.position.x;
    sr.y = r.position.y;
    sr.w = r.size.x;
    sr.h = r.size.y;
    return sr;
}

void renderRect(SDL_Renderer* renderer, bite::Rectangle& r)
{
    auto sr = toSdlRect(r);
    SDL_SetRenderDrawColor(renderer, r.color.r, r.color.g, r.color.b, 255);
    SDL_RenderFillRect(renderer, &sr);
}  

void render(SDL_Renderer* renderer, Rectangles rs)
{
    Uint8 r, g, b;

    /* Clear the screen */
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);

    /*  Come up with a random rectangle */
    SDL_Rect rect;
    rect.w = randomInt(64, 128);
    rect.h = randomInt(64, 128);
    rect.x = randomInt(0, SCREEN_WIDTH);
    rect.y = randomInt(0, SCREEN_HEIGHT);

    /* Come up with a random color */
    r = randomInt(50, 255);
    g = randomInt(50, 255);
    b = randomInt(50, 255);
    SDL_SetRenderDrawColor(renderer, r, g, b, 255);
    
    /*  Fill the rectangle in the color */
    // SDL_RenderFillRect(renderer, &rect);

    using namespace std;
    for_each(begin(rs), end(rs), bind(renderRect, renderer, placeholders::_1));

    /* update screen */
    SDL_RenderPresent(renderer);
}




std::ostream& operator<< (std::ostream& s, Vec2 v)
{
    s << "x = " << v.x << " ; y = " << v.y << " \n";
    return s;
}

std::ostream& operator<< (std::ostream& s, const bite::Rectangle r)
{
    s << "pos: " << r.position << "size: " << r.size;
    return s;
}

std::ostream& operator<< (std::ostream& s, Rectangles r)
{
    using namespace std;
    //std::copy(begin(r), end(r), std::ostream_iterator<bite::Rectangle>(s, ", "));
    return s;
}

template<typename T>
auto genVec2(T&& f)
{
    return Vec2(f(), f());
};


auto genRandList(int w, int h)
{
    using namespace std;
    random_device rd;
    mt19937 engineMt(rd());
    uniform_real_distribution<float> unif_x(0.f, SCREEN_WIDTH - w);
    uniform_real_distribution<float> unif_y(0.f, SCREEN_HEIGHT - h);
    uniform_int_distribution<int> uniform_int_wh(20, min(w, h));
    uniform_int_distribution<unsigned> unif_color(0, 255);

    auto random_color = bind(unif_color, engineMt);
    auto random_x = bind(unif_x, engineMt);
    auto random_y = bind(unif_y, engineMt);
    auto random_wh = bind(uniform_int_wh, engineMt);

    static const unsigned sizeVec{ 5 };
    Rectangles myList(sizeVec);
    auto genRect = [&]() {
        Color c{(uint8_t)random_color(), (uint8_t)random_color(), (uint8_t)random_color()};
        return bite::Rectangle(Vec2(random_x(), random_y()), genVec2(random_wh), c);
    };

    generate(begin(myList), end(myList), genRect);
    return myList;
}

#ifndef NETWORK
// Local input
void readInput()
{
    using namespace std;
    while (true)
    {
        string text;
        cout << "chibi>";

        getline(cin, text);
        if (text.length() > 0)
        {
            auto ret3 = schemeEval(text);
            // cout << text << "\n";
            cout << ret3 << "\n";
        }
    }
}

#endif

#ifdef NETWORK

#include "retry_client_endpoint.h"

using Client = websocketpp::retry_client_endpoint<websocketpp::retry_config<websocketpp::config::asio_client>> ;
websocketpp::lib::shared_ptr<websocketpp::lib::thread> test_thread;
Client test_client;
websocketpp::connection_hdl test_con_hdl;

void on_open(Client* c, websocketpp::connection_hdl hdl)
{
    std::cout << "Connected..." << std::endl;
    test_con_hdl = hdl;
    sema_connect.signal();
}


void on_message(Client* c, websocketpp::connection_hdl hdl, ws::message_ptr msg)
{
    using namespace std;

    websocketpp::lib::error_code ec;
    Client::connection_ptr con = c->get_con_from_hdl(hdl , ec);
    using namespace std;
    cout << "Message received:" << endl;
    auto text = msg->get_payload();
    cout << text << endl;
    string res = "empty";
    if (text != "hello")
    {
        res = schemeEval(text);
        cout << res << "/n";
    }
    //cout << con->get_resource()  << endl;
    // con->send("received!");
    con->send(res);
}

void configure_con(Client* c, websocketpp::connection_hdl hdl)
{
    // Do all our necessary configurations before attempting to connect
    Client::connection_ptr con = c->get_con_from_hdl(hdl);
    std::cout << "Connection Attempt: " << con->m_attempt_count << std::endl;

    con->set_open_handler(bind(
                               &on_open,
                               c,
                               std::placeholders::_1));
    
    con->set_message_handler(bind(  
                                  &on_message,
                                  c,
                                  std::placeholders::_1, std::placeholders::_2
                                    ));

}


void connectToWs()
{
    using namespace std;

    //string uri = "ws://localhost:9002";
    //string uri = "ws://192.168.0.20:9002";
    //string uri = "ws://192.168.10.196:9002";
    string uri = "ws://10.43.108.114:9002"; // 10.43.108.114

    std::cout << "Setting up connection to attempt to connect to: " << uri << std::endl;

    // Remove superfluous logging
    test_client.set_access_channels(websocketpp::log::alevel::all);
    test_client.set_error_channels(websocketpp::log::elevel::all);

    // Normal endpoint setup (just as you would with the regular websocketpp::client)
    test_client.init_asio();

    // The endpoint must be perpetual. TODO look at supporting non perpetual (will have to use .reset())
    test_client.start_perpetual();
    // Start spinning the thread
    test_thread.reset(new websocketpp::lib::thread(&Client::run, &test_client));

    // Done boilerplate initialization, now our connection code:
    websocketpp::lib::error_code ec;
    Client::connection_ptr con = test_client.get_connection(uri, ec);

    /// Everything up to here has been standard websocket++ stuff
    /// But now, this is unique settings to tell our endpoint to retry
    con->m_retry = true;        // Indicates that we do want to attempt to retry connecting (if first attempt fails)
    con->m_retry_delay = 2000;   // Will wait 400ms between delays
    con->m_max_attempts = 10;    // Only make 3 attempts.
    con->set_configure_handler(bind(&configure_con, &test_client, std::placeholders::_1));

    // Now connect will start attempting to connect
    test_client.connect(con);

    // Sleep so there's time to retry if necessary.
    // std::this_thread::sleep_for(std::chrono::milliseconds(1500));
    sema_connect.wait();

    // It's important to note that we can't use the original connection ptr from above, because
    // when retrying, it must get a new connection each time in the helper class
    // so the actual connection ptr that is successfully connection might be different from
    // the one we started with. You can see in on_open(...) we assign the test_con_hdl
    // which we know is our successfull connection ptr.
    con = test_client.get_con_from_hdl(test_con_hdl, ec);
    if(ec)
    {
        // Couldn't get a connection_ptr from the connection_hdl (weak_ptr), so it either never connected, or connection was already closed.
        std::cout << "Not connected!\n";
    }
    else
    {
        // con->close(websocketpp::close::status::going_away,"");

        // Give a bit of time for the close message to be sent
        // std::this_thread::sleep_for(std::chrono::milliseconds(700));
        // std::cout << "Connection closed" << std::endl;
    }

    sema.signal();
}

#endif

//#undef main
int main(int argc, char *argv[])
{
    using namespace std;
    
#ifdef NETWORK
    thread connectThread(connectToWs);
    sema.wait();
#endif
    
    auto rs = genRandList(150, 270);
    

    ctx = sexp_make_eval_context(NULL, NULL, NULL, 0, 0);
    sexp_load_standard_env(ctx, NULL, SEXP_SEVEN);
    sexp_load_standard_ports(ctx, NULL, stdin, stdout, stderr, 0);

    // Registering our C functions
    sexp_init_library(ctx, NULL, 3, sexp_context_env(ctx), sexp_version, SEXP_ABI_IDENTIFIER);

    auto rectangles = buildList(ctx, rs);
    // Create global
    auto obj3 = sexp_intern(ctx, "my-rects", -1); 
    sexp_env_define(ctx, sexp_context_env(ctx), obj3, rectangles);

    dostuff(ctx);
    
    int done;
    SDL_Event event;

    /* initialize SDL */
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Could not initialize SDL\n");
        return 1;
    }

    /* seed random number generator */
    srand(time(NULL));

    /* create window and renderer */
    auto window = SDL_CreateWindow(NULL, 10, 10, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_OPENGL);
    if (!window) 
    {
        printf("Could not initialize Window\n");
        return 1;
    }

    auto renderer = SDL_CreateRenderer(window, -1, 0);
    if (!renderer) 
    {
        printf("Could not create renderer\n");
        return 1;
    }

#ifndef NETWORK
    thread t(readInput);
#endif
    
    /* Enter render loop, waiting for user to quit */
    done = 0;
    while (!done)
    {
        while (SDL_PollEvent(&event)) 
        {
            if (event.type == SDL_QUIT) 
            {
                done = 1;
            }
        }
        // string text;
        // cin >> text;
        render(renderer, rs);
        SDL_Delay(100);
    }

#ifdef NETWORK
    //t.join();
    connectThread.join();
#endif
    
    /* shutdown SDL */
    SDL_Quit();

    sexp_destroy_context(ctx);
    return 0;
}
