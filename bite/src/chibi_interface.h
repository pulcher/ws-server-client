#include <string>

#include "chibi/eval.h"
#include <algorithm>
#include <functional>
#include <iterator>
#include <random>
#include <vector>

#include "my_cstructs.h"

// Traits to map C++ type to Chibi's name
template <typename T>
struct chibiCT {
};

template <>
struct chibiCT<bite::Vec2>
{
    static constexpr const char* kName = "Vec2";    
};

template <>
struct chibiCT<bite::Rectangle>
{
    static constexpr const char* kName = "Rectangle";
};

// Builds a list fprom c++ containers
template <typename C>
sexp buildList(sexp ctx, C& container)
{
    // Declare and preserve local variables
    sexp_gc_var3(obj1, objList, obj3);
    sexp_gc_preserve3(ctx, obj1, objList, obj3);

    obj1 = sexp_c_string(ctx, chibiCT<typename C::value_type>::kName, -1);
    obj1 = sexp_lookup_type(ctx, obj1, SEXP_FALSE);
    auto typeTag = sexp_type_tag(obj1);

    // We convert from the back as 'cons' will add new elements from the beginning
    auto it = std::rbegin(container);
    // Before adding to list we must create sexp pointer
    obj3 = sexp_make_cpointer(ctx, sexp_type_tag(obj1), &(*it), SEXP_FALSE, 0);
    objList = sexp_cons(ctx, obj3, SEXP_NULL);

    while (it != std::rend(container)) {
        obj3 = sexp_make_cpointer(ctx, typeTag, &*it, SEXP_FALSE, 0);
        objList = sexp_cons(ctx, obj3, objList);
        ++it;
    }

    // Make sure our newly created list is not deleted
    sexp_preserve_object(ctx, objList);
    sexp_gc_release3(ctx);
    return objList;
}
