#pragma once

#ifndef MY_STRUCTS
#define MY_STRUCTS

#include <string>
#include <cstdint>

#include "chibi/eval.h"

#include "Rectangle.h"

#ifdef __cplusplus
extern "C" {
#endif

    void hello_user(const char *name);
    void goodbye();

    sexp sexp_init_library (sexp ctx, sexp self, sexp_sint_t n, sexp env, const char* version, const sexp_abi_identifier_t abi);

#ifdef __cplusplus
}
#endif



// testing returning copy
glm::vec2 getPos(bite::Rectangle* r);



#endif
