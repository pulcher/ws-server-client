

#pragma once


#include <glm/glm.hpp>
/*
struct Vec2
{
  Vec2() = default;
  Vec2(float x, float y) : x(x), y(y) {};
  Vec2(int x, int y) : x(x), y(y) {};
  float x,y;
  void  setBoth(int x, int y)
  {
    x = x;
    y = y;
  }
  static constexpr const char* kName = "Vec2";
};
*/

namespace bite
{

using Vec2 = glm::vec2;

struct Color
{
	uint8_t r, g, b;
	Color() = default;
	Color(uint8_t r, uint8_t g, uint8_t b) : r(r), g(g), b(b) {};
};

struct Rectangle
{
	Rectangle() = default;
	Rectangle(glm::vec2 position, glm::vec2 size, Color color)
		: position(position), size(size), color(color) {};

	glm::vec2 position;
	glm::vec2 size;
	struct Color color;

	const char* name;
	unsigned id;
};

}