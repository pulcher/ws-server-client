#include "client.h"

int main() {
    using namespace ws;

    bool done = false;
    std::string input;
    Client client;

    while (!done) {
        std::cout << "Enter Command: ";
        std::getline(std::cin, input);

        if (input == "quit") {
            done = true;
        }
        else if (input == "help") {
            std::cout
                << "\nCommand List:\n"
                << "connect <ws uri>\n"
                << "close <connection id> [<close code:default=1000>] [<close reason>]\n"
                << "show <connection id>\n"
                << "help: Display this help text\n"
                << "quit: Exit the program\n"
                << std::endl;
        } else if (input.substr(0,7) == "connect") {
            int id = client.connect(input.substr(8));
            if (id != -1) {
                std::cout << "> Created connection with id " << id << std::endl;
            }
        } else if (input.substr(0,5) == "close") {
            std::stringstream ss(input);
            
            std::string cmd;
            int id;
            int close_code = websocketpp::close::status::normal;
            std::string reason = "";
            
            ss >> cmd >> id >> close_code;
            std::getline(ss,reason);
            
            client.close(id, close_code, reason);
        }
        else if (input.substr(0,4) == "show")
        {
            int id = atoi(input.substr(5).c_str());

            ConnectionMeta::ptr metadata = client.get_metadata(id);
            if (metadata) {
                std::cout << *metadata << std::endl;
            } else {
                std::cout << "> Unknown connection id " << id << std::endl;
            }
        }
        else if (input.substr(0, 4) == "send")
        {
            client.sendRaw(input.substr(5));
        }
        else if (input.substr(0, 4) == "sbin")
        {
            // send binary test data
            int a[] = {1,2,3,1024,5,6,7,8,9,10,11};
            client.sendBin(a, 10*sizeof(int));
        }
        else 
        {
            std::cout << "> Unrecognized Command" << std::endl;
        }
    }

    return 0;
}


