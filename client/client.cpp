#include "client.h"

/* -------------------------------------------------------------------------- */


namespace ws {

/* -------------------------------------------------------------------------- */

void ConnectionMeta::on_message(client* c, websocketpp::connection_hdl hdl, message_ptr msg)
{
    websocketpp::lib::error_code ec;
    client::connection_ptr con = c->get_con_from_hdl(hdl , ec);
    using namespace std;
    cout << "Message received:" << endl;
    cout << msg->get_payload() << endl;
    //cout << con->get_resource()  << endl;

	/*
    // parse message
    string err;
    auto json = json11::Json::parse(msg->get_payload(), err);
    gMessageQueueJson.put(json);
	*/

	//c->send(hdl, "{hello : abc}", msg->get_opcode(), ec);
	//std::string const & payload = "{hello : abc}";
	//con->send(payload, msg->get_opcode());
}

void ConnectionMeta::on_open(client * c, websocketpp::connection_hdl hdl)
{
    websocketpp::lib::error_code ec;
    status_ = "Open";

    client::connection_ptr con = c->get_con_from_hdl(hdl, ec);
    server_ = con->get_response_header("Server");
}

void ConnectionMeta::on_fail(client * c, websocketpp::connection_hdl hdl)
{
    websocketpp::lib::error_code ec;
    
    status_ = "Failed";
    std::cout << "Failed! \n";

    client::connection_ptr con = c->get_con_from_hdl(hdl, ec);
    server_ = con->get_response_header("Server");
    error_reason_ = con->get_ec().message();
}

void ConnectionMeta::on_close(client * c, websocketpp::connection_hdl hdl)
{
    websocketpp::lib::error_code ec;
    
    status_ = "Closed";
    client::connection_ptr con = c->get_con_from_hdl(hdl, ec);
    std::stringstream s;
    s << "close code: " << con->get_remote_close_code() << " (" 
      << websocketpp::close::status::get_string(con->get_remote_close_code()) 
      << "), close reason: " << con->get_remote_close_reason();
    error_reason_ = s.str();
}

std::ostream & operator<< (std::ostream & out, ConnectionMeta const & data)
{
    out << "> URI: " << data.uri_ << "\n"
        << "> Status: " << data.status_ << "\n"
        << "> Remote Server: " << (data.server_.empty() ? "None Specified" : data.server_) << "\n"
        << "> Error/close reason: " << (data.error_reason_.empty() ? "N/A" : data.error_reason_);

    return out;
}

/* -------------------------------------------------------------------------- */


Client::Client () : next_id_(0) 
{
    endpoint_.clear_access_channels(websocketpp::log::alevel::all);
    endpoint_.clear_error_channels(websocketpp::log::elevel::all);

    websocketpp::lib::error_code ec;
    
    endpoint_.init_asio(ec);
    endpoint_.start_perpetual();

    // thread_ = websocketpp::lib::make_shared<websocketpp::lib::thread>(&client::run, &endpoint_);
    thread_.reset(new websocketpp::lib::thread(&client::run, &endpoint_));
}


Client::~Client() 
{
    endpoint_.stop_perpetual();
	
    for (con_list::const_iterator it = connection_list_.begin(); it != connection_list_.end(); ++it)
    {
        if (it->second->get_status() != "Open")
        {
            // Only close open connections
            continue;
        }
		
        std::cout << "> Closing connection " << it->second->get_id() << std::endl;
		
        websocketpp::lib::error_code ec;
        endpoint_.close(it->second->get_hdl(), websocketpp::close::status::going_away, "", ec);
        if (ec)
        {
            std::cout << "> Error closing connection " << it->second->get_id() << ": "  
                      << ec.message() << std::endl;
        }
    }
	
    thread_->join();
}


/* -------------------------------------------------------------------------- */

void Client::sendBin(void* data, size_t len)
{
    auto m = connection_list_[0];
    
    websocketpp::lib::error_code ec;
    client::connection_ptr con = endpoint_.get_con_from_hdl(m->get_hdl(), ec);
    
    con->send(data, len);
}
/* -------------------------------------------------------------------------- */

void Client::sendRaw(const std::string& msg)
{
    auto m = connection_list_[0];
    websocketpp::lib::error_code ec;
    client::connection_ptr con = endpoint_.get_con_from_hdl(m->get_hdl(), ec);
    
	//endpoint_.send(m->get_hdl(), msg, 1);
	//std::string const & payload = "{hello : abc}";
	//con->send(msg, 0x1);
    
    //!!!!!!!!!!
    con->send(msg);
    //c->send(m->get_hdl(), msg, 1, ec);

}


/* -------------------------------------------------------------------------- */

int Client::connect(std::string const & uri) 
{
    websocketpp::lib::error_code ec;

    client::connection_ptr con = endpoint_.get_connection(uri, ec);

    if (ec)
    {
        std::cout << "> Connect initialization error: " << ec.message() << std::endl;
        return -1;
    }

    int new_id = next_id_++;
    ConnectionMeta::ptr metadata_ptr = websocketpp::lib::make_shared<ConnectionMeta>(new_id, con->get_handle(), uri);
    connection_list_[new_id] = metadata_ptr;

    con->set_open_handler(websocketpp::lib::bind(
                                                 &ConnectionMeta::on_open,
                                                 metadata_ptr,
                                                 &endpoint_,
                                                 websocketpp::lib::placeholders::_1
                                                 ));

    con->set_message_handler(websocketpp::lib::bind(
                                                    &ConnectionMeta::on_message,
                                                    metadata_ptr,
                                                    &endpoint_,
                                                    websocketpp::lib::placeholders::_1,websocketpp::lib::placeholders::_2
                                                    ));

    con->set_fail_handler(websocketpp::lib::bind(
                                                 &ConnectionMeta::on_fail,
                                                 metadata_ptr,
                                                 &endpoint_,
                                                 websocketpp::lib::placeholders::_1
                                                 ));
    con->set_close_handler(websocketpp::lib::bind(
                                                  &ConnectionMeta::on_close,
                                                  metadata_ptr,
                                                  &endpoint_,
                                                  websocketpp::lib::placeholders::_1
                                                  ));

    endpoint_.connect(con);

    return new_id;
}

/* -------------------------------------------------------------------------- */

void Client::close(int id, websocketpp::close::status::value code, std::string reason) {
    websocketpp::lib::error_code ec;
	
    con_list::iterator metadata_it = connection_list_.find(id);
    if (metadata_it == connection_list_.end()) {
        std::cout << "> No connection found with id " << id << std::endl;
        return;
    }
	
    endpoint_.close(metadata_it->second->get_hdl(), code, reason, ec);
    if (ec) {
        std::cout << "> Error initiating close: " << ec.message() << std::endl;
    }
}

/* -------------------------------------------------------------------------- */

ConnectionMeta::ptr Client::get_metadata(int id) const 
{
    con_list::const_iterator metadata_it = connection_list_.find(id);
    if (metadata_it == connection_list_.end()) {
        return ConnectionMeta::ptr();
    } else {
        return metadata_it->second;
    }
}

/* -------------------------------------------------------------------------- */


} // namespace ws

/* -------------------------------------------------------------------------- */
