/* -------------------------------------------------------------------------- */

#include <memory>
#include <set>
#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>


namespace ws {

/* -------------------------------------------------------------------------- */

using WsServer = websocketpp::server<websocketpp::config::asio>;
using WsConnection = WsServer::connection_ptr;

using websocketpp::connection_hdl;

class Server
{
public:
  /* -------------------------------------------------------------------------- */
    class Listener
    {
        friend class Server;
    public:
        virtual ~Listener() {}
        virtual void onMessage(WsConnection con, WsServer::message_ptr msg) = 0;
        virtual void onConnect(WsConnection con) {};
        virtual void onDisconnect(WsConnection con) {};
        Server* getServer() const { return server_; }

    private:
        Server* server_{nullptr};
    };
    /* -------------------------------------------------------------------------- */

public:
    Server();
    ~Server();

    bool onValidate(connection_hdl hdl);
    void onOpen(connection_hdl hdl);
    void onClose(connection_hdl hdl);
    void onMessage(connection_hdl hdl, WsServer::message_ptr msg);
    void stop() { server_.stop(); }
    void run(uint16_t port)
    {
        server_.listen(port);
        server_.start_accept();
        server_.run();
    }

    // Send message to the first client
    void sendMessage(const std::string& msg);
    void broadcastMessage(const std::string& msg);
    void setListener(Listener* listener);

    WsConnection getConnectionFromHandle(connection_hdl);
private:
    using con_list = std::set<connection_hdl,std::owner_less<connection_hdl>>;
    WsServer    server_;
    con_list    connections_;
    std::unique_ptr<Listener> listener_;
    bool        isBinary_;
};

/* -------------------------------------------------------------------------- */

} // namespace ws

/* -------------------------------------------------------------------------- */


