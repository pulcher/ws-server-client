#include "server.h"

/* -------------------------------------------------------------------------- */

using namespace ws;

using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;
using websocketpp::lib::bind;

/* -------------------------------------------------------------------------- */

Server::Server() : isBinary_{false}
{
    using websocketpp::log::alevel;
    using websocketpp::log::elevel;

    // Remove superfluous logging
    server_.set_access_channels(alevel::none);
    server_.set_error_channels(elevel::none);

    server_.init_asio();
    server_.set_reuse_addr(true);
    server_.set_open_handler(bind(&Server::onOpen,this,::_1));
    server_.set_close_handler(bind(&Server::onClose,this,::_1));
    server_.set_message_handler(bind(&Server::onMessage,this,::_1,::_2));
    server_.set_validate_handler(bind(&Server::onValidate,this,::_1));
}

/* -------------------------------------------------------------------------- */

Server::~Server()
{
    // setListener(nullptr);
}

/* -------------------------------------------------------------------------- */

bool Server::onValidate(connection_hdl hdl)
{
    if (auto con = getConnectionFromHandle(hdl))
    {
        auto const& prots = con->get_requested_subprotocols();
        if(!prots.size())
            return true;

        con->select_subprotocol(prots.front());
        isBinary_ = (prots.front() == "binary");

        return true;
    }

    return false;
}

/* -------------------------------------------------------------------------- */

void Server::onOpen(connection_hdl hdl)
{
    // Register connection if not already being tracked, and notify listener
    const auto pair = connections_.insert(hdl);
    if (pair.second && listener_)
        listener_->onConnect(getConnectionFromHandle(hdl));
}

/* -------------------------------------------------------------------------- */

void Server::onClose(connection_hdl hdl)
{
    if (connections_.erase(hdl) && listener_)
        listener_->onDisconnect(getConnectionFromHandle(hdl));
}

/* -------------------------------------------------------------------------- */

void Server::onMessage(connection_hdl hdl, WsServer::message_ptr msg)
{
    if (listener_)
        listener_->onMessage(getConnectionFromHandle(hdl), msg);
}

/* -------------------------------------------------------------------------- */

void Server::sendMessage(const std::string& msg)
{
    // first connection
    auto c = connections_.begin();
    if (c != connections_.end())
    {
        auto con = server_.get_con_from_hdl(*c);
        if(isBinary_)
            con->send(msg, websocketpp::frame::opcode::binary);
        else
            con->send(msg);
    }
    else
    {
        std::cout << "No connection to send data to!\n";
    }
}

/* -------------------------------------------------------------------------- */

void Server::broadcastMessage(const std::string& msg)
{
    if(!connections_.size())
    {
        std::cout << "No connections to broadcast data to!\n";
        return;
    }

    for(auto con_hdl: connections_)
    {
        auto con = server_.get_con_from_hdl(con_hdl);
        if(isBinary_)
            con->send(msg, websocketpp::frame::opcode::binary);
        else
            con->send(msg);
    }
}

/* -------------------------------------------------------------------------- */

void Server::setListener(Listener* listener)
{
    if (listener)
    {
        listener_.reset(listener);
        listener_->server_ = this;
    }
}

/* -------------------------------------------------------------------------- */

WsConnection Server::getConnectionFromHandle(connection_hdl hdl)
{
    websocketpp::lib::error_code ec;
    return server_.get_con_from_hdl(hdl, ec);
}

/* -------------------------------------------------------------------------- */



